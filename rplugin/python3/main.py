import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.parent.parent / 'python3'))

import neovim

from config import Config
from providers.gitlab import GitlabProject


@neovim.plugin
class VimIssues:
    def __init__(self, nvim: neovim.Nvim) -> None:
        self.nvim = nvim
        self.config = Config(self.nvim.funcs.getcwd())

    @neovim.function("ReloadConfig", sync=True)
    def reload(self, args):
        self.config = Config(self.nvim.funcs.getcwd())
        print(self.config)
        return 'some string'

    # @neovim.command('Issues', range='', nargs='*')
    # def issues(self, args, range_):
    #     issues = self.project.get_issues(8, 2)
    #     formatted = self.project.format_issues(issues)
    #     self.nvim.current.buffer[:] = formatted
