import subprocess
from abc import ABC, abstractclassmethod, abstractmethod
from typing import Dict, List, Optional

from .constants import VcsSoftware


class VcsConfig:
    executable: str
    upstream: str
    name: VcsSoftware

    def __init__(self, executable: str, upstream: str, name: VcsSoftware):
        self.executable = executable
        self.upstream = upstream
        self.name = name

    @classmethod
    def create(cls, config_data: Dict, default: 'VcsConfig') -> 'VcsConfig':
        return VcsConfig(
            executable=config_data.get('executable', default.executable),
            upstream=config_data.get('upstream', default.upstream),
            name=default.name
        )


class VCS(ABC):
    default_config: 'VcsConfig'
    config: 'VcsConfig'

    @classmethod
    def create(cls, name: str, config: Dict):
        if name == VcsSoftware.git.name:
            return Git(config)

    @abstractmethod
    def active_branch(self) -> Optional[str]:
        pass

    @abstractmethod
    def root_path(self) -> Optional[str]:
        pass

    @abstractmethod
    def is_valid(self) -> bool:
        pass


class Git(VCS):
    default_config = VcsConfig('git', 'origin', VcsSoftware.git)

    def __init__(self, config=None):
        self.config = VcsConfig.create(config or {}, self.default_config)

    def active_branch(self):
        resp = subprocess.run([self.config.executable, 'rev-parse', '--abbrev-ref', 'HEAD'], stdout=subprocess.PIPE)
        if resp.returncode == 0:
            return resp.stdout.decode().strip()

    def root_path(self):
        resp = subprocess.run([self.config.executable, 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE)
        if resp.returncode == 0:
            return resp.stdout.decode().strip()

    def is_valid(self):
        resp = subprocess.run([self.config.executable, 'rev-parse', '--abbrev-ref', 'HEAD'], stdout=subprocess.PIPE)
        return resp.returncode == 0

    def remotes(self):
        pass


#
# class Hg(VCS):
#     default_branch = 'default'
#     type_ = VcsSoftware.hg
#     executable = 'hg'
#
#     @property
#     @classmethod
#     def active_branch(cls):
#         resp = subprocess.run([cls.executable, 'identity', '-b'], stdout=subprocess.PIPE)
#         if resp.returncode == 0:
#             return resp.stdout.decode().strip()
#
#     @property
#     @classmethod
#     def root_path(cls):
#         resp = subprocess.run([cls.executable, 'root'], stdout=subprocess.PIPE)
#         if resp.returncode == 0:
#             return resp.stdout.decode().strip()

version_control_systems: List[VCS] = [Git]
