import subprocess
from typing import Optional, List, Dict, Tuple
from pathlib import Path
import json
import itertools

from .vcs import version_control_systems, VCS
from .constants import VcsSoftware, IssuesSoftware, ReviewSoftware, CiSoftware


class Config:
    config_name = '.project.json'
    apis: List['ApiConfig']
    projects: List['ProjectConfig']
    vcs: List[VCS]

    def __init__(self, cwd: str):
        self.config_file = self.get_config_file(Path(cwd))
        self.load()

    def load(self):
        self.vcs = []
        self.apis = []
        self.projects = []
        if self.config_file is not None:
            self.parse_config()

        self.complete()

    def parse_config(self):
        if self.config_file is None:
            return

        with self.config_file.open('r') as fp:
            config_data = json.load(fp)
            self.vcs = [VcsSoftware.create(name, d) for name, d in config_data.get('vcs', {}).items()]
            self.apis = [ApiConfig(d) for d in config_data.get('apis', [])]
            self.projects = [ProjectConfig(d) for d in config_data.get('projects', [])]

    def complete(self):
        if len(self.vcs) == 0:
            vcs = [v() for v in version_control_systems if v]
            self.vcs = list(filter(lambda v: v.is_valid(), vcs))

        if len(self.vcs) == 0:
            raise RuntimeError('No vcs system found')



    @classmethod
    def get_config_file(cls, cwd: Path) -> Optional[Path]:
        home = Path.home() # TODO: unix only for the moment
        for directory in itertools.chain([cwd], cwd.parents):
            if directory < home:
                return None

            config_path = directory / cls.config_name
            if config_path.exists():
                return config_path

        return None


class ApiConfig:
    branch: str
    token: Optional[str]
    type_: str
    url: str
    vcs: str

    def __init__(self, data: dict):
        self.branch = data['branch'] or 'master'
        self.token = data['token']
        self.type_ = data['type']
        self.url = data['url']
        self.vcs = data['vcs']


class IssuesConfig:
    type_: IssuesSoftware
    api: ApiConfig

    def __init__(self, config):
        self.type_ = config.get('type')
        self.api = ApiConfig(config.get('api'))


class ReviewsConfig:
    type_: IssuesSoftware
    api: ApiConfig

    def __init__(self, config):
        self.type_ = config.get('type')
        self.api = ApiConfig(config.get('api'))


class CiConfig:
    type_: IssuesSoftware
    api: ApiConfig

    def __init__(self, config):
        self.type_ = config.get('type')
        self.api = ApiConfig(config.get('api'))


class ProjectConfig:
    vcs: str
    branch: str
    issues: IssuesConfig
    reviews: ReviewsConfig
    ci: CiConfig

    def __init__(self, config):
        pass
